<%-- 
    Document   : resultados
    Created on : 29/11/2021, 07:49:33 PM
    Author     : David
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultados</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container pt-4 col-md-6">
            <h1 class="text-center">Resultados de la Consulta</h1>
            <table class="table">
                <tr>
                    <th>Emisor:</th>
                    <td>${re}</td>
                </tr>
                <tr>
                    <th>Receptor:</th>
                    <td>${rr}</td>
                </tr>
                <tr>
                    <th>Total:</th>
                    <td>${tt}</td>
                </tr>
                <tr>
                    <th>UUID:</th>
                    <td>${uuid}</td>
                </tr>
                <tr>
                    <th>Resultado:</th>
                    <td>
                        <c:choose>
                            <c:when test="${resultado=='Vigente'}">
                                <span class="badge badge-success p-2">
                                    ${resultado}
                                </span>
                            </c:when>
                            <c:otherwise>
                                <span class="badge badge-danger p-2">
                                    ${resultado}
                                </span>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </table>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js"></script>
    </body>
</html>
