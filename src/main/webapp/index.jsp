<%-- 
    Document   : index
    Created on : 29/11/2021, 07:32:23 PM
    Author     : David
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sat Validator</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container pt-4 col-md-4">
            <h1 class="text-center">Validación de UUID en SAT</h1>
            <form action="validar" method="POST">
                <div class="row">
                    <div class="col">
                        <form-group>
                            <label for="re">Emisor</label>
                            <input type="text" class="form-control" name="re" value="LSO1306189R5">
                        </form-group>
                    </div>
                    <div class="col">
                        <form-group>
                            <label for="re">Receptor</label>
                            <input type="text" class="form-control" name="rr" value="GACJ940911ASA">
                        </form-group>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <form-group>
                            <label for="re">Total</label>
                            <input type="text" class="form-control" name="tt" value="4999.99">
                        </form-group>
                    </div>
                    <div class="col">
                        <form-group>
                            <label for="re">UUID</label>
                            <input type="text" class="form-control" name="uuid" value="e7df3047-f8de-425d-b469-37abe5b4dabb">
                        </form-group>
                    </div>
                </div>
                <div class="row">
                    <div class="col p-3">
                        <form-group>
                            <button type="submit" class="btn btn-success btn-block">Enviar</button>
                        </form-group>
                    </div>
                </div>
            </form>

        </div>

        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js"></script>
    </body>
</html>
