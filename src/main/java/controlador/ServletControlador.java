/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.SatValidator;

/**
 *
 * @author David
 */
@WebServlet("/validar")
public class ServletControlador extends HttpServlet {
    
    private static String resultado;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String re = request.getParameter("re");
        String rr = request.getParameter("rr");
        String tt = request.getParameter("tt");
        String uuid = request.getParameter("uuid");
        
        resultado = new SatValidator().valildateUUID(re, rr, tt, uuid);
        
        request.setAttribute("re", re);
        request.setAttribute("rr", rr);
        request.setAttribute("tt", tt);
        request.setAttribute("uuid", uuid);
        request.setAttribute("resultado", resultado);
        
        request.getRequestDispatcher("vistas/resultados.jsp").forward(request, response);
    }
}
