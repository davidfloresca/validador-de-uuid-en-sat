package models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author David
 */
public class SatValidator {
    
    private String re;
    private String rr;
    private String tt;
    private String uuid;
    private static HttpURLConnection connection;
    private static String resultado;

    public SatValidator() {
    }

    public SatValidator(String re, String rr, String tt, String uuid) {
        this.re = re;
        this.rr = rr;
        this.tt = tt;
        this.uuid = uuid;
    }

    public String getRe() {
        return re;
    }

    public void setRe(String re) {
        this.re = re;
    }

    public String getRr() {
        return rr;
    }

    public void setRr(String rr) {
        this.rr = rr;
    }

    public String getTt() {
        return tt;
    }

    public void setTt(String tt) {
        this.tt = tt;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    public String valildateUUID(String re, String rr, String tt, String uuid) throws IOException{
        try {
            URL url = new URL("https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl");
            connection = (HttpURLConnection) url.openConnection();

            //request setup
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-type", "text/xml;charset=\"utf-8\"");
            connection.setRequestProperty("Accept", "text/xml");
            connection.setRequestProperty("SOAPAction", "http://tempuri.org/IConsultaCFDIService/Consulta");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.setDoOutput(true);

            
            String rawInputString = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body><tem:Consulta><tem:expresionImpresa><![CDATA[?re="+re+"&rr="+rr+"&tt="+tt+"&id="+uuid+"]]></tem:expresionImpresa></tem:Consulta></soapenv:Body></soapenv:Envelope>";

            try (OutputStream os = connection.getOutputStream()) {
                byte[] input = rawInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            int status = connection.getResponseCode();
            System.out.println("status = " + status);

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(connection.getInputStream(), "utf-8"))) {
                StringBuilder respuesta;
                respuesta = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    respuesta.append(responseLine.trim());
                }
                String out = respuesta.toString();
                if (out.contains("Vigente")) {
                    resultado = "Vigente";
                } else if (out.contains("No Encontrado")) {
                    resultado = "No Encontrado";
                } else {
                    System.out.println(out);
                    resultado = out;
                }

            }

        } catch (MalformedURLException e) {
            System.out.println("e = " + e.getMessage());
        } finally {
            connection.disconnect();
        }
        return resultado;
    }
    
}
